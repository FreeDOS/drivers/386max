# 386MAX

This is the initial import of the 386MAX project. It was released under the
GNU General Public License, version 3 at https://github.com/sudleyplace/386MAX
on July 3, 2022. However, the compiled executables do not display the new
license information. They also require installation using the setup program and
require providing a user name and serial number.

It has not undergone any testing and has not been converted for easy
installation as a FreeDOS package.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## 386MAX.LSM

<table>
<tr><td>title</td><td>386MAX</td></tr>
<tr><td>version</td><td>8.03.006</td></tr>
<tr><td>entered&nbsp;date</td><td>2023-02-20</td></tr>
<tr><td>description</td><td>386MAX Memory Manager for DOS</td></tr>
<tr><td>summary</td><td>386MAX Memory Manager for DOS. Must install sources and execute compiled installer. Code still needs updated to reflect license change and forego the need for entering a user name and serial number.</td></tr>
<tr><td>keywords</td><td>memory</td></tr>
<tr><td>author</td><td>Qualitas, Sudley Place Software, Bob Smith</td></tr>
<tr><td>original&nbsp;site</td><td>https://github.com/sudleyplace/386MAX</td></tr>
<tr><td>platforms</td><td>DOS, Windows</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, version 3](LICENSE)</td></tr>
</table>
