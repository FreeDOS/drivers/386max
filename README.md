This is the initial import of the 386MAX project. It was released under the
GNU General Public License, version 3 at https://github.com/sudleyplace/386MAX
on July 3, 2022. However, the compiled executables do not display the new
license information. They also require installation using the setup program and
require providing a user name and serial number.

It has not undergone any testing and has not been converted for easy
installation as a FreeDOS package.
